package com.coupon.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulServer;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName GatewayApplication.java
 * @create 2023年06月23日 下午8:42
 * @Description Zuul-Gateway组件
 * @Version V1.0
 */
@SpringCloudApplication
@EnableZuulServer
public class GatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }
}
