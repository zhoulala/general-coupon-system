package com.coupon.gateway.filter;

import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName AbstractPostZuulFilter.java
 * @create 2023年06月23日 下午9:15
 * @Description 后置抽象过滤器类
 * @Version V1.0
 */
public abstract class AbstractPostZuulFilter extends AbstractZuulFilter{

    @Override
    public String filterType() {
        return FilterConstants.POST_TYPE;
    }
}
