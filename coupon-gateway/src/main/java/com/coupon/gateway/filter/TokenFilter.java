package com.coupon.gateway.filter;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName TokenFilter.java
 * @create 2023年06月23日 下午9:41
 * @Description Token检验过滤器
 * @Version V1.0
 */
@Component
@Slf4j
@SuppressWarnings("all")
public class TokenFilter extends AbstractPreZuulFilter {

    @Override
    protected Object cRun() {
        HttpServletRequest request = requestContext.getRequest();
        log.info("请求的方法类型：{},请求的方法地址：{}", request.getMethod(), request.getRequestURL());

        String token = request.getParameter("token");

        if (StringUtils.isEmpty(token)) {
            log.error("error : token is isEmpty");
            return fail(401, "error : token is isEmpty");
        }

        return success();
    }

    @Override
    public int filterOrder() {
        return 1;
    }
}
