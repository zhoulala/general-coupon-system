package com.coupon.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName TemplateApplication.java
 * @create 2023年06月24日 下午8:20
 * @Description 模板微服务的启动类
 * @Version V1.0
 */
@EnableEurekaClient
@SpringBootApplication
@EnableScheduling
@EnableJpaAuditing
public class TemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(TemplateApplication.class, args);
    }

}
