package com.coupon.template.service;

import com.coupon.template.entity.CouponTemplate;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName AsyncService.java
 * @create 2023年06月25日 下午3:02
 * @Description 异步服务接口定义
 * @Version V1.0
 */
public interface AsyncService {

    /**
     * 根据模板异步创建优惠券码
     * @param template {@link CouponTemplate} 优惠券模板实体
     */
    void asyncConstructCouponByTemplate(CouponTemplate template);

}
