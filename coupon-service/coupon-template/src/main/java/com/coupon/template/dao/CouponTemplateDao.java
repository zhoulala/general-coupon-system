package com.coupon.template.dao;

import com.coupon.template.entity.CouponTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponTemplateDao.java
 * @create 2023年06月25日 下午2:01
 * @Description 优惠券模板数据层
 * @Version V1.0
 */
public interface CouponTemplateDao
        extends JpaRepository<CouponTemplate, Integer> {

    // 根据模板名称查询模板
    CouponTemplate findByName(String name);

    // 根据 available 和 expired 标记查找模板记录
    List<CouponTemplate> findAllByAvailableAndExpired(
            Boolean available, Boolean expired
    );

    // 根据 expired 标记查找模板记录
    List<CouponTemplate> findAllByExpired(Boolean expired);

}
