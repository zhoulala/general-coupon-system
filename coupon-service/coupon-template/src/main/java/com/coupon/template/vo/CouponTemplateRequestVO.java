package com.coupon.template.vo;

import com.coupon.common.constant.CouponCategoryEnum;
import com.coupon.common.constant.DistributeTargetEnum;
import com.coupon.common.constant.ProductLineEnum;
import com.coupon.common.vo.TemplateRule;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName TemplateRequest.java
 * @create 2023年06月25日 下午2:15
 * @Description 优惠券模板创建请求对象
 * @Version V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CouponTemplateRequestVO {

    // 优惠券名称
    private String name;

    // 优惠券logo
    private String logo;

    // 优惠券描述
    private String desc;

    // 优惠券分类
    private String category;

    // 产品线
    private Integer productLine;

    // 总数
    private Integer count;

    // 创建用户
    private Long userId;

    // 目标用户
    private Integer target;

    // 优惠券规则
    private TemplateRule rule;

    // 校验对象的合法性
    public boolean validate() {
        boolean stringValid = StringUtils.isNotEmpty(name)
                && StringUtils.isNotEmpty(logo)
                && StringUtils.isNotEmpty(desc);
        boolean enumValid = null != CouponCategoryEnum.of(category)
                && null != ProductLineEnum.of(productLine)
                && null != DistributeTargetEnum.of(target);
        boolean numValid = count > 0 && userId > 0;
        return stringValid && enumValid && numValid && rule.validate();
    }

}
