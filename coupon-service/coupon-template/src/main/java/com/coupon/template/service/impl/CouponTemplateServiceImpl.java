package com.coupon.template.service.impl;

import com.coupon.common.exception.CouponException;
import com.coupon.template.dao.CouponTemplateDao;
import com.coupon.template.entity.CouponTemplate;
import com.coupon.template.service.AsyncService;
import com.coupon.template.service.CouponTemplateService;
import com.coupon.template.vo.CouponTemplateRequestVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponTemplateServiceImpl.java
 * @create 2023年06月25日 下午4:20
 * @Description 构建优惠券模板接口实现类
 * @Version V1.0
 */
@Slf4j
@Service
public class CouponTemplateServiceImpl implements CouponTemplateService {

    @Autowired
    private AsyncService asyncService;

    @Autowired
    private CouponTemplateDao couponTemplateDao;

    @Override
    public CouponTemplate buildTemplate(CouponTemplateRequestVO template)
            throws CouponException {
        // 参数合法性校验
        if (!template.validate()) {
            throw new CouponException("BuildTemplate Param Is Not Valid!");
        }
        // 判断同名的优惠券模板是否存在
        if (null != couponTemplateDao.findByName(template.getName())) {
            throw new CouponException("Exist Same Name Template!");
        }
        // 构造 CouponTemplate 并保存到数据库中
        CouponTemplate couponTemplate = request2Template(template);
        couponTemplate = couponTemplateDao.save(couponTemplate);

        // 根据优惠券模板异步生成优惠券码
        asyncService.asyncConstructCouponByTemplate(couponTemplate);

        return couponTemplate;
    }

    // CouponTemplateRequestVO -> CouponTemplate
    private CouponTemplate request2Template(CouponTemplateRequestVO template) {
        return new CouponTemplate(template.getName(),
                template.getLogo(),
                template.getDesc(),
                template.getCategory(),
                template.getProductLine(),
                template.getCount(),
                template.getUserId(),
                template.getTarget(),
                template.getRule());
    }

}
