package com.coupon.template.converter;

import com.coupon.common.constant.CouponCategoryEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponCategoryConverter.java
 * @create 2023年06月25日 上午11:22
 * @Description 优惠券分类枚举类型转换器
 * @Version V1.0
 */
@Converter
public class CouponCategoryConverter implements
        AttributeConverter<CouponCategoryEnum,String> {
    // 将实体属性X转换为Y存储到数据库中，插入和更新时执行的动作
    @Override
    public String convertToDatabaseColumn(CouponCategoryEnum couponCategoryEnum) {
        return couponCategoryEnum.getCode();
    }
    // 将数据库中的字段Y转换为实体属性X，查询操作时执行的动作
    @Override
    public CouponCategoryEnum convertToEntityAttribute(String code) {
        return CouponCategoryEnum.of(code);
    }
}
