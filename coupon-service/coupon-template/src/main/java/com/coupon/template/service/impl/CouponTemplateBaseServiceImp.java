package com.coupon.template.service.impl;

import com.coupon.common.exception.CouponException;
import com.coupon.common.vo.CouponTemplateSDK;
import com.coupon.template.dao.CouponTemplateDao;
import com.coupon.template.entity.CouponTemplate;
import com.coupon.template.service.CouponTemplateBaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponTemplateBaseServiceImp.java
 * @create 2023年06月25日 下午4:35
 * @Description 优惠券模板基础服务实现
 * @Version V1.0
 */
@Slf4j
@Service
public class CouponTemplateBaseServiceImp implements CouponTemplateBaseService {

    @Autowired
    private CouponTemplateDao couponTemplateDao;

    // 根据优惠券模板id获取优惠券模板信息
    @Override
    public CouponTemplate getTemplateInfo(Integer id) throws CouponException {
        Optional<CouponTemplate> template = couponTemplateDao.findById(id);
        if (!template.isPresent()) {
            throw new CouponException("Template is not exist: " + id);
        }
        return template.get();
    }
    // 获取所有可用的优惠券模板
    @Override
    public List<CouponTemplateSDK> findAllUsableTemplate() throws CouponException {
        List<CouponTemplate> templates =
                couponTemplateDao.findAllByAvailableAndExpired(true, false);

        return templates.stream()
                .map(this::template2TemplateSDK)
                .collect(Collectors.toList());
    }
    // 获取模板ids到CouponTemplateSDK的映射
    @Override
    public Map<Integer, CouponTemplateSDK> findIds2TemplateSDK(Collection<Integer> ids) {

        List<CouponTemplate> templates = couponTemplateDao.findAllById(ids);

        return templates.stream()
                .map(template -> template2TemplateSDK(template))
                .collect(Collectors.toMap(
                        CouponTemplateSDK::getId,
                        Function.identity()
                ));
    }

    // CouponTemplate -> CouponTemplateSDK
    private CouponTemplateSDK template2TemplateSDK(CouponTemplate template) {

        return new CouponTemplateSDK(template.getId(),
                template.getName(),
                template.getLogo(),
                template.getDesc(),
                template.getCategory().getCode(),
                template.getProductLine().getCode(),
                template.getKey(),// 并不是拼装好的 Template Key
                template.getTarget().getCode(),
                template.getRule());
    }

}
