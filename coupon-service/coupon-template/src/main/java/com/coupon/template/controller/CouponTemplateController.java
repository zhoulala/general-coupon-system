package com.coupon.template.controller;

import com.alibaba.fastjson.JSON;
import com.coupon.common.exception.CouponException;
import com.coupon.common.vo.CouponTemplateSDK;
import com.coupon.template.entity.CouponTemplate;
import com.coupon.template.service.CouponTemplateBaseService;
import com.coupon.template.service.CouponTemplateService;
import com.coupon.template.vo.CouponTemplateRequestVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponTemplateController.java
 * @create 2023年06月25日 下午8:36
 * @Description 优惠券模板Controller
 * @Version V1.0
 */
@Slf4j
@RestController
public class CouponTemplateController {

    // 优惠券模板服务
    @Autowired
    private CouponTemplateService couponTemplateService;
    // 优惠券模板基础服务
    @Autowired
    private CouponTemplateBaseService couponTemplateBaseService;

    // 构建优惠券模板
    // url: localhost:7001/coupon-template/template/build
    // zuul: http://localhost:9000/coupon/coupon-template/template/build
    @PostMapping("/template/build")
    public CouponTemplate buildTemplate(@RequestBody CouponTemplateRequestVO request)
            throws CouponException {

        log.info("Build Template: {}", JSON.toJSON(request));

        return couponTemplateService.buildTemplate(request);
    }

    // 构造优惠券模板详情
    // url: localhost:7001/coupon-template/template/info?id=1
    // zuul: http://localhost:9000/coupon/coupon-template/template/info?id=1
    @GetMapping("/template/info")
    public CouponTemplate getTemplateInfo(@RequestParam Integer id)
            throws CouponException {
        log.info("Get Template Info For: {}", id);
        return couponTemplateBaseService.getTemplateInfo(id);
    }


    // 查找所有可用的优惠券模板
    // url: localhost:7001/coupon-template/template/sdk/all
    // zuul: http://localhost:9000/coupon/coupon-template/template/sdk/all
    @GetMapping("/template/sdk/all")
    public List<CouponTemplateSDK> findAllUsableTemplate()
            throws CouponException {
        log.info("Find All Usable Template");
        return couponTemplateBaseService.findAllUsableTemplate();
    }

    // 获取模板ids到CouponTemplateSDK的映射
    // url: localhost:7001/coupon-template/template/sdk/infos
    // zuul: http://localhost:9000/coupon/coupon-template/template/sdk/infos
    @GetMapping("/template/sdk/infos")
    public Map<Integer, CouponTemplateSDK> findIds2TemplateSDK(@RequestParam List<Integer> ids) {
        log.info("FindIds2TemplateSDK: {}", JSON.toJSONString(ids));
        return couponTemplateBaseService.findIds2TemplateSDK(ids);
    }

}
