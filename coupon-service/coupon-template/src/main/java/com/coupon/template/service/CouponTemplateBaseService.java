package com.coupon.template.service;

import com.coupon.common.exception.CouponException;
import com.coupon.common.vo.CouponTemplateSDK;
import com.coupon.template.entity.CouponTemplate;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponTemplateBaseService.java
 * @create 2023年06月25日 下午3:15
 * @Description 优惠券模板基础服务定义
 * @Version V1.0
 */

public interface CouponTemplateBaseService {

    // 根据优惠券模板id获取优惠券模板信息
    CouponTemplate getTemplateInfo(Integer id) throws CouponException;

    // 查找所有可用的优惠券模板
    List<CouponTemplateSDK> findAllUsableTemplate() throws CouponException;

    // 获取模板ids到CouponTemplateSDK的映射
    Map<Integer, CouponTemplateSDK> findIds2TemplateSDK(Collection<Integer> ids);

}
