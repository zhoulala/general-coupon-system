package com.coupon.template.converter;

import com.alibaba.fastjson.JSON;
import com.coupon.common.vo.TemplateRule;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName TemplateRuleConverter.java
 * @create 2023年06月25日 上午11:34
 * @Description 优惠券模板规则转换类
 * @Version V1.0
 */
@Converter
public class TemplateRuleConverter implements
        AttributeConverter<TemplateRule, String> {

    @Override
    public String convertToDatabaseColumn(TemplateRule templateRule) {
        return JSON.toJSONString(templateRule);
    }

    @Override
    public TemplateRule convertToEntityAttribute(String rule) {
        return JSON.parseObject(rule, TemplateRule.class);
    }
}
