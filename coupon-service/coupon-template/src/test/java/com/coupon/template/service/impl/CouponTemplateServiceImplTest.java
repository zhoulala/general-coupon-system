package com.coupon.template.service.impl;

import com.alibaba.fastjson.JSON;
import com.coupon.common.constant.CouponCategoryEnum;
import com.coupon.common.constant.DistributeTargetEnum;
import com.coupon.common.constant.PeriodTypeEnum;
import com.coupon.common.constant.ProductLineEnum;
import com.coupon.common.exception.CouponException;
import com.coupon.common.vo.TemplateRule;
import com.coupon.template.service.CouponTemplateService;
import com.coupon.template.vo.CouponTemplateRequestVO;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponTemplateServiceImplTest.java
 * @create 2023年06月25日 下午9:36
 * @Description
 * @Version V1.0
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class CouponTemplateServiceImplTest {

    @Autowired
    private CouponTemplateService couponTemplateService;

    @Test
    public void buildTemplate() throws CouponException, InterruptedException {
        System.out.println(JSON.toJSONString(
                couponTemplateService.buildTemplate(fakeTemplateRequestVO())
        ));
        Thread.sleep(5000);
    }

    // 构造CouponTemplateRequestVO
    private CouponTemplateRequestVO fakeTemplateRequestVO() {
        CouponTemplateRequestVO requestVO = new CouponTemplateRequestVO();
        requestVO.setName("优惠券模板-" + new Date().getTime());
        requestVO.setLogo("http://www.imooc.com");
        requestVO.setDesc("这是一张优惠券模板");
        requestVO.setCategory(CouponCategoryEnum.ZHEKOU.getCode());
        requestVO.setProductLine(ProductLineEnum.DAMAO.getCode());
        requestVO.setCount(10000);
        requestVO.setUserId(10001L);
        requestVO.setTarget(DistributeTargetEnum.SINGLE.getCode());

        TemplateRule templateRule = new TemplateRule();
        templateRule.setExpiration(new TemplateRule.Expiration(
                PeriodTypeEnum.SHIFT.getCode(), 1, DateUtils.addDays(new Date(), 60).getTime()
        ));
        templateRule.setDiscount(new TemplateRule.Discount(
                5, 1
        ));
        templateRule.setUsage(new TemplateRule.Usage(
                "安徽省", "桌面", JSON.toJSONString(Arrays.asList("文具", "电脑"))
        ));
        templateRule.setLimitation(1);
        templateRule.setWeight(JSON.toJSONString(Collections.EMPTY_LIST));

        requestVO.setRule(templateRule);

        return requestVO;
    }

}