package com.coupon.template.service.impl;

import com.alibaba.fastjson.JSON;
import com.coupon.common.exception.CouponException;
import com.coupon.common.vo.CouponTemplateSDK;
import com.coupon.template.entity.CouponTemplate;
import com.coupon.template.service.CouponTemplateBaseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponTemplateBaseServiceImpTest.java
 * @create 2023年06月25日 下午10:09
 * @Description 优惠券模板基础服务测试程序
 * @Version V1.0
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class CouponTemplateBaseServiceImpTest {

    @Autowired
    private CouponTemplateBaseService couponTemplateBaseService;

    @Test
    public void getTemplateInfo() throws CouponException {
        CouponTemplate templateInfo = couponTemplateBaseService.getTemplateInfo(10);
        CouponTemplate templateInfo2 = couponTemplateBaseService.getTemplateInfo(2);
        System.out.println("templateInfo = " + JSON.toJSONString(templateInfo));
        System.out.println("templateInfo = " + JSON.toJSONString(templateInfo2));
    }

    @Test
    public void findAllUsableTemplate() throws CouponException {
        List<CouponTemplateSDK> allUsableTemplate = couponTemplateBaseService.findAllUsableTemplate();
        allUsableTemplate.forEach(b -> System.out.println(JSON.toJSONString(b)));
    }

    @Test
    public void findIds2TemplateSDK() {
        Map<Integer, CouponTemplateSDK> ids2TemplateSDK = couponTemplateBaseService.findIds2TemplateSDK(Arrays.asList(10));
        System.out.println("ids2TemplateSDK = " + ids2TemplateSDK);
    }
}