package com.coupon.template;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName TemplateApplicationTest.java
 * @create 2023年06月25日 下午9:24
 * @Description 模板系统测试程序
 * @Version V1.0
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class TemplateApplicationTest {

    @Test
    public void contextLoad() {

    }

}
