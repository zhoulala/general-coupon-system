package com.coupon.common.conf;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName JacksonConfig.java
 * @create 2023年06月24日 下午4:05
 * @Description Jackson 的自定义配置
 * @Version V1.0
 */
@Configuration
public class JacksonConfig {

    private final String PATTERN = "yyyy-MM-dd HH:mm:ss";

    @Bean
    public ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.setDateFormat(new SimpleDateFormat(PATTERN));

        return objectMapper;
    }

}
