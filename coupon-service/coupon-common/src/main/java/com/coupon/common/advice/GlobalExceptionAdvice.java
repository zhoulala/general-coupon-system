package com.coupon.common.advice;

import com.coupon.common.exception.CouponException;
import com.coupon.common.vo.CommonResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName GlobalExceptionAdvice.java
 * @create 2023年06月24日 下午6:41
 * @Description 全局异常增强
 * @Version V1.0
 */
@RestControllerAdvice
public class GlobalExceptionAdvice {

    @ExceptionHandler(value = CouponException.class)
    public CommonResponse<String> handlerCouponException(HttpServletRequest request,
                                                         CouponException e){
        CommonResponse<String> response = new CommonResponse<>(-1,"business error");

        response.setData(e.getMessage());

        return response;
    }

}
