package com.coupon.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName DistributeTargetEnum.java
 * @create 2023年06月25日 上午10:04
 * @Description 优惠券分发目标枚举
 * @Version V1.0
 */
@Getter
@AllArgsConstructor
public enum DistributeTargetEnum {
    // 单用户 -> 用户需要自行领取
    SINGLE("单用户", 1),
    // 多用户 -> 优惠券系统主动推送给用户
    MULTI("多用户", 2);

    // 分发目标描述
    private String description;
    // 分发目标编码
    private Integer code;

    // 根据code获取到DistributeTargetEnum
    public static DistributeTargetEnum of(Integer code) {
        Objects.requireNonNull(code);

        return Stream.of(values())
                .filter(bean -> bean.code.equals(code))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(code + " not exists!"));
    }

}
