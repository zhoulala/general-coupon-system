package com.coupon.common.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName PeriodTypeEnum.java
 * @create 2023年06月25日 上午10:13
 * @Description 有效期类型枚举
 * @Version V1.0
 */
@Getter
@AllArgsConstructor
public enum PeriodTypeEnum {

    // 固定日期
    REGULAR("固定日期", 1),
    // 限时天数
    SHIFT("限时天数", 2);

    // 有效期描述
    private String description;
    // 有效期编码
    private Integer code;

    // 根据code获取PeriodTypeEnum
    public static PeriodTypeEnum of(Integer code) {
        Objects.requireNonNull(code);

        return Stream.of(values())
                .filter(bean -> bean.code.equals(code))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(code + " not exists!"));
    }

}
