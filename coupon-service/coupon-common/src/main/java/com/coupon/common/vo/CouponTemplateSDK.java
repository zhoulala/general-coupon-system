package com.coupon.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponTemplateSDK.java
 * @create 2023年06月25日 下午3:09
 * @Description 微服务之间传输的优惠券模板定义
 * @Version V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CouponTemplateSDK {

    // 优惠券模板主键
    private Integer id;

    // 优惠券模板名称
    private String name;

    // 优惠券logo
    private String logo;

    // 优惠券描述
    private String desc;

    // 优惠券分类
    private String category;

    // 产品线
    private Integer productLine;

    // 优惠券模板的编码 由于我们的优惠券模板是由Kafka生产的，所以我们需要一个唯一的编码来标识优惠券模板
    // 优惠券模板的唯一编码 = 4(产品线和类型) + 8(日期：20230625) + id(扩充为4位)
    private String key;

    // 目标用户
    private Integer target;

    // 优惠券规则
    private TemplateRule rule;

}
