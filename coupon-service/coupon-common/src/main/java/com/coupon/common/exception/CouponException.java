package com.coupon.common.exception;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponException.java
 * @create 2023年06月24日 下午6:39
 * @Description 优惠券通用异常
 * @Version V1.0
 */

public class CouponException extends Exception{

    public CouponException(String message){
        super(message);
    }



}
