package com.coupon.common.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName WebConfiguration.java
 * @create 2023年06月24日 下午3:58
 * @Description Web通用配置类
 * @Version V1.0
 */
//定制 HTTP 消息转换器
@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.clear();

        converters.add(new MappingJackson2HttpMessageConverter());

    }
}
