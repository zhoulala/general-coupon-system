package com.coupon.settlement.controller;

import com.alibaba.fastjson.JSON;
import com.coupon.common.exception.CouponException;
import com.coupon.common.vo.SettlementInfo;
import com.coupon.settlement.executor.ExecutorManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName SettlementController.java
 * @create 2023年06月27日 下午4:08
 * @Description 优惠券结算服务的Controller
 * @Version V1.0
 */
@Slf4j
@RestController
public class SettlementController {

    // 结算规则执行管理器
    @Autowired
    private ExecutorManager executorManager;

    /**
     * 优惠券结算
     * @param settlementInfo 包含了选择的优惠券
     * @return SettlementInfo
     * @throws CouponException 优惠券异常
     */
    // url: http://localhost:7003/coupon-settlement/settlement/compute
    // zuul: http://localhost:9000/coupon/coupon-settlement/settlement/compute
    @PostMapping("/settlement/compute")
    public SettlementInfo computeRule(@RequestBody SettlementInfo settlementInfo)
            throws CouponException {
        log.info("settlement: {}", JSON.toJSONString(settlementInfo));
        return executorManager.computeRule(settlementInfo);
    }


}
