package com.coupon.settlement.executor.impl;

import com.coupon.common.vo.CouponTemplateSDK;
import com.coupon.common.vo.SettlementInfo;
import com.coupon.settlement.constant.RuleFlagEnum;
import com.coupon.settlement.executor.AbstractExecutor;
import com.coupon.settlement.executor.RuleExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName ZheKouExecutor.java
 * @create 2023年06月27日 下午2:33
 * @Description 折扣优惠券规则执行器
 * @Version V1.0
 */
@Slf4j
@Component
public class ZheKouExecutor extends AbstractExecutor implements RuleExecutor {
    /**
     * 规则类型标记
     *
     * @return {@link RuleFlagEnum}
     */
    @Override
    public RuleFlagEnum ruleConfig() {
        return RuleFlagEnum.ZHEKOU;
    }

    /**
     * 优惠券规则计算
     *
     * @param settlement {@link SettlementInfo} 包含了选择的优惠券
     * @return {@link SettlementInfo} 修正过的结算信息
     */
    @Override
    public SettlementInfo computeRule(SettlementInfo settlement) {
        // 获取商品总价
        double goodsCost = retain2Decimals(goodsCostSum(settlement.getGoodsInfos()));

        SettlementInfo settlementInfo = processGoodsTypeNotSatisfy(settlement, goodsCost);

        if (settlementInfo!=null){
            log.debug("ZheKou Template Is Not Match GoodsType!");
            return settlementInfo;
        }
        // 折扣优惠券可以直接使用，没有门槛
        CouponTemplateSDK templateSDK = settlement.getCouponAndTemplateInfos().get(0).getTemplateSDK();
        // 获取折扣额度
        double quota = templateSDK.getRule().getDiscount().getQuota();

        // 计算使用优惠券之后的价格
        settlement.setCost(
                retain2Decimals(goodsCost * (quota * 1.0 / 100)) > minCost() ?
                        retain2Decimals(goodsCost * (quota * 1.0 / 100)) : minCost()
        );

        log.debug("Use ZheKou Coupon Make Goods Cost From {} To {}",
                goodsCost,settlement.getCost());

        return settlement;
    }
}
