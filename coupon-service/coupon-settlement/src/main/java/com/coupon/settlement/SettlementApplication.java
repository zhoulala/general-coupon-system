package com.coupon.settlement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName SettlementApplication.java
 * @create 2023年06月27日 上午10:54
 * @Description 优惠券结算微服务启动类
 * @Version V1.0
 */
@SpringBootApplication
@EnableEurekaClient
public class SettlementApplication {
    public static void main(String[] args) {
        SpringApplication.run(SettlementApplication.class, args);
    }
}
