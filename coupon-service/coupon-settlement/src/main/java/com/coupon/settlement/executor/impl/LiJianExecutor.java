package com.coupon.settlement.executor.impl;

import com.coupon.common.vo.CouponTemplateSDK;
import com.coupon.common.vo.SettlementInfo;
import com.coupon.settlement.constant.RuleFlagEnum;
import com.coupon.settlement.executor.AbstractExecutor;
import com.coupon.settlement.executor.RuleExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName LiJianExecutor.java
 * @create 2023年06月27日 下午2:43
 * @Description 立减优惠券规则执行器
 * @Version V1.0
 */
@Slf4j
@Component
public class LiJianExecutor extends AbstractExecutor implements RuleExecutor {
    /**
     * 规则类型标记
     *
     * @return {@link RuleFlagEnum}
     */
    @Override
    public RuleFlagEnum ruleConfig() {
        return RuleFlagEnum.LIJIAN;
    }

    /**
     * 优惠券规则计算
     *
     * @param settlement {@link SettlementInfo} 包含了选择的优惠券
     * @return {@link SettlementInfo} 修正过的结算信息
     */
    @Override
    public SettlementInfo computeRule(SettlementInfo settlement) {

        // 获得商品总价
        double goodsSum = retain2Decimals(goodsCostSum(
                settlement.getGoodsInfos()
        ));
        // 判断商品类型与优惠券是否匹配
        SettlementInfo probability = processGoodsTypeNotSatisfy(
                settlement, goodsSum
        );
        if (null != probability) {
            log.debug("LiJian Template Is Not Match To GoodsType!");
            return probability;
        }
        // 立减优惠券直接使用，没有门槛
        CouponTemplateSDK templateSDK = settlement.getCouponAndTemplateInfos().get(0).getTemplateSDK();
        double quota = (double) templateSDK.getRule().getDiscount().getQuota();
        // 计算使用优惠券之后的价格 - 结算
        settlement.setCost(
                goodsSum - quota > minCost() ? (goodsSum - quota) : minCost()
        );
        log.debug("Use LiJian Coupon Make Goods Cost From {} To {}",
                goodsSum, settlement.getCost());

        return settlement;
    }
}
