package com.coupon.distribution.entity;

import com.coupon.common.vo.CouponTemplateSDK;
import com.coupon.distribution.constant.CouponStatusEnum;
import com.coupon.distribution.converter.CouponStatusEnumConverter;
import com.coupon.distribution.serialization.CouponSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName Coupon.java
 * @create 2023年06月26日 上午9:37
 * @Description 优惠券实体表(用户领取的优惠券记录)
 * @Version V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "coupon")
@JsonSerialize(using = CouponSerialize.class)
public class Coupon {

    // 自增主键
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    // 关联优惠券模板的主键(逻辑外键)
    @Column(name = "template_id", nullable = false)
    private Integer templateId;

    // 领取用户
    @Column(name = "user_id", nullable = false)
    private Long userId;

    // 优惠券码
    @Column(name = "coupon_code", nullable = false)
    private String couponCode;

    // 领取时间
    @CreatedDate
    @Column(name = "assign_time", nullable = false)
    private Date assignTime;

    // 优惠券状态
    @Column(name = "status", nullable = false)
    @Convert(converter = CouponStatusEnumConverter.class)
    private CouponStatusEnum status;

    // 用户优惠券对应的模板信息
    @Transient
    private CouponTemplateSDK couponTemplateSDK;

    // 返回一个无效的Coupon对象
    public static Coupon invalidCoupon() {
        Coupon coupon = new Coupon();
        coupon.setId(-1);
        return coupon;
    }

    // 构造优惠券
    public Coupon(Integer templateId, Long userId, String couponCode, CouponStatusEnum status) {
        this.templateId = templateId;
        this.userId = userId;
        this.couponCode = couponCode;
        this.status = status;
    }

}
