package com.coupon.distribution.dao;

import com.coupon.distribution.constant.CouponStatusEnum;
import com.coupon.distribution.entity.Coupon;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponDao.java
 * @create 2023年06月26日 上午10:13
 * @Description 优惠券数据访问层
 * @Version V1.0
 */
public interface CouponDao extends JpaRepository<Coupon, Integer> {

    // 根据userId + 状态寻找优惠券记录
    List<Coupon> findAllByUserIdAndStatus(Long userId, CouponStatusEnum status);

}
