package com.coupon.distribution.feign;

import com.coupon.common.exception.CouponException;
import com.coupon.common.vo.CommonResponse;
import com.coupon.common.vo.SettlementInfo;
import com.coupon.distribution.feign.hystrix.SettlementClientHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName SettlementClient.java
 * @create 2023年06月26日 下午5:56
 * @Description 优惠券结算微服务Feign接口定义
 * @Version V1.0
 */
@FeignClient(value = "eureka-client-coupon-settlement",
                fallback = SettlementClientHystrix.class)
public interface SettlementClient {

    /**
     * 优惠券规则计算
     */
    @PostMapping("/coupon-settlement/settlement/compute")
    CommonResponse<SettlementInfo> computeRule(@RequestBody SettlementInfo settlementInfo)
            throws CouponException;

}
