package com.coupon.distribution.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName KafkaService.java
 * @create 2023年06月26日 上午10:45
 * @Description Kafka 相关的服务接口定义
 * @Version V1.0
 */
public interface KafkaService {

    // 消费优惠券 Kafka 消息
    void consumeCouponKafkaMessage(ConsumerRecord<?, ?> record);

}
