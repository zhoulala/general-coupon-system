package com.coupon.distribution.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponKafkaMessageVO.java
 * @create 2023年06月26日 下午3:51
 * @Description 优惠券kafka消息对象
 * @Version V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CouponKafkaMessageVO {

    // 优惠券状态
    private Integer status;

    // Coupon主键
    private List<Integer> ids;

}
