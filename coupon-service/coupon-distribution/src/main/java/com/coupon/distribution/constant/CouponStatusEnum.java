package com.coupon.distribution.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName CouponStatus.java
 * @create 2023年06月26日 上午9:30
 * @Description 优惠券状态信息枚举 - 用户优惠券状态
 * @Version V1.0
 */
@Getter
@AllArgsConstructor
public enum CouponStatusEnum {

    // 优惠券状态
    USABLE("可用的", 1),
    USED("已使用的", 2),
    EXPIRED("过期的(未被使用的)", 3);

    // 优惠券状态描述信息
    private String description;

    // 优惠券状态编码
    private Integer code;

    // 根据code获取到CouponStatusEnum
    public static CouponStatusEnum of(Integer code) {
        Objects.requireNonNull(code);
        return Stream.of(values())
                .filter(bean -> bean.code.equals(code))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(code + " not exists!")
                );
    }

}
