package com.coupon.distribution.feign.hystrix;

import com.coupon.common.exception.CouponException;
import com.coupon.common.vo.CommonResponse;
import com.coupon.common.vo.SettlementInfo;
import com.coupon.distribution.feign.SettlementClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName SettlementClientHystrix.java
 * @create 2023年06月26日 下午7:40
 * @Description 优惠券结算微服务Feign接口 调用熔断降级策略
 * @Version V1.0
 */
@Slf4j
@Component
public class SettlementClientHystrix implements SettlementClient {
    /**
     * 优惠券规则计算
     *
     * @param settlementInfo
     */
    @Override
    public CommonResponse<SettlementInfo> computeRule(SettlementInfo settlementInfo)
            throws CouponException {
        log.error("[eureka-client-coupon-settlement] computeRule " +
                "request error");
        settlementInfo.setEmploy(false);
        settlementInfo.setCost(-1.0);
        return new CommonResponse<>(-1,
                "[eureka-client-coupon-settlement] computeRule " +
                        "request error",
                settlementInfo);
    }
}
