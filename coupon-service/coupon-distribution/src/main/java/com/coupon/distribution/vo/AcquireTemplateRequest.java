package com.coupon.distribution.vo;

import com.coupon.common.vo.CouponTemplateSDK;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 王哲
 * @Contact 1121586359@qq.com
 * @ClassName AcquireTemplateRequest.java
 * @create 2023年06月26日 上午10:57
 * @Description 获取优惠券请求对象定义
 * @Version V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AcquireTemplateRequest {

    // 用户id
    private Long userId;

    // 优惠券模板信息
    private CouponTemplateSDK templateSDK;

}
